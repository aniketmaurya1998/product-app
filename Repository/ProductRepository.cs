﻿using ProductApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Repository
{
    internal class ProductRepository
    {
        Product[] products;
        public ProductRepository()
        {
            products = new Product[]
            {
                new Product("Oppo", "Mobile", 15000, 3.5f),
                new Product("Samsung", "TV", 35000, 4.5f),
                new Product("Mi", "Mobile", 10000,4f)
            };
        }
        public Product[] GetAllProducts()
        {
            return products;

        }
        public Product[] GetProductsByCategory(string category)
        {
            return Array.FindAll(products, product => product.Category == category);
        }
        public Product[] GetProductsByName(string name)
        {
            return Array.FindAll(products, product => product.Name == name);
        }

        public Product[] UpdateProduct(string name, int price)
        {
            int idx = Array.FindIndex(products, product => product.Name == name);
            if (idx >= 0)
            {
                products[idx].Price = price;
            }
            return products;
        }
        public Product[] DelProduct( string name)
        {
            return Array.FindAll(products, product => product.Name != name);
        }
        public Product[] AddProduct(Product product)
        {
            products.Append(product);   
            return products;
        }
    }
}
